# Routing
- Unabhänig von Psr 7

- kann auch für andere Methoden benutzt werden

## Umfasst:
- Router

- Dispatcher

- Collector für Routes, Middleware und Strategy

- Route Generator

<br>

[hier gehts weiter mit: Router](router.md)

### Inhalt Routing
[1. Start](index.md) <br>
[2. Router](router.md) <br>
[3. Dispatching](dispatching.md) <br>
[4. Route Creation](routeCreation.md) <br>
[5. Route Generation](routegeneration.md)
